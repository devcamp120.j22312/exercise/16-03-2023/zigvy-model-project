const mongoose = require('mongoose');

const postModel = require('../models/postModel');

// Create Post
const createPost = (request, response) => {
    const body = request.body;

    //
    if (!body.title) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Title is not valid'
        })
    }
    if (!body.body) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Body is not valid'
        })
    }

    let newPost = {
        _id: mongoose.Types.ObjectId(),
        userId: mongoose.Types.ObjectId(),
        title: body.title,
        body: body.body
    }
    postModel.create(newPost, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        return response.status(200).json({
            status: 'Post Created',
            data: data
        })
    })
}

// 
const getAllPost = (request, response) => {
    let userId = request.query.userId;

    let condition = {};

    if(userId) {
        condition.userId = userId;
    }

    postModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        return response.status(200).json({
            status: 'Success: Get All Post',
            data: data
        })
    })
}

const getPostById = (request, response) => {
    const postId = request.params.postId;

    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Post ID is invalid'
        })
    }

    postModel.findById(postId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        return response.status(200).json({
            status: `Got Post #${postId}`,
            data: data
        })
    })
}

// update post by id 
const updatePostById = (request, response) => {
    const postId = request.params.postId;
    const body = request.body;

    // 
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Post ID is invalid'
        })
    }
    if (!body.title) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Title is not valid'
        })
    }
    if (!body.body) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Body is not valid'
        })
    }

    const updatePost = {};

    if (body.title !== undefined) {
        updatePost.title = body.title;
    }
    if (body.body !== undefined) {
        updatePost.body = body.body;
    }
    postModel.findByIdAndUpdate(postId, updatePost, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        return response.status(200).json({
            status: `Got Post #${postId}`,
            data: data
        })
    })
}

// Delete Post By Id
const deletePostById = (request, response) => {
    const postId = request.params.postId;

    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'PostId is not valid'
        })
    }

    postModel.findByIdAndDelete(postId, (error) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        return response.status(200).json({
            status: 'Post Deleted'
        })
    })
}

// Get Post Of User
const getPostsOfUser = (request, response) => {
    const userId = request.params.userId;

    let condition = {};

    if(userId) {
        condition.userId = userId;
    }

    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'User ID is not valid'
        })
    }


    postModel.findById(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        return response.status(200).json({
            status: 'Get All Post Of User',
            data: data
        })
    })
}

module.exports = {
    createPost,
    getAllPost,
    getPostById,
    updatePostById,
    deletePostById,
    getPostsOfUser
}