const mongoose = require('mongoose');

const albumModel = require('../models/albumModel');

const createAlbum = (request, response) => {
    let bodyRequest = request.body;

    if (!bodyRequest.userId) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'UserID is not valid'
        })
    }
    if (!bodyRequest.title) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Title is not valid'
        })
    }

    let newAlbum = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyRequest.userId,
        title: bodyRequest.title
    }
    albumModel.create(newAlbum, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Success: Create Album successfully',
                data: data
            })
        }
    })
}

const getAllAlbum = (request, response) => {
    let userId = request.query.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId
    }

    albumModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Success: Get All Album',
                data: data
            })
        }
    })
}

const getAlbumById = (request, response) => {
    let albumId = request.params.albumId;

    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Album ID is not valid'
        })
    }

    albumModel.findById(albumId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Get Album ${albumId}`,
                data: data
            })
        }
    })
}

const updateAlbumById = (request, response) => {
    let albumId = request.params.albumId;
    let bodyRequest = request.body;

    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Album ID is not valid'
        })
    }
    if (!bodyRequest.userId) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'UserID is not valid'
        })
    }
    if (!bodyRequest.title) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Title is not valid'
        })
    }

    let updateAlbum = {
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        body: bodyRequest.body
    }

    albumModel.findByIdAndUpdate(albumId, updateAlbum, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Update Album ${albumId}`,
                data: data
            })
        }
    })
}

const deleteAlbumById = (request, response) => {
    let albumId = request.params.albumId;

    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Album ID is not valid'
        })
    }

    albumModel.findByIdAndDelete(albumId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Delete Album ${albumId}`,
            })
        }
    })
}

// Get Albums of User
const getAlbumOfUser = (request, response) => {
    let userId = request.params.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Album ID is not valid'
        })
    }

    albumModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Get All Album Of User ID: ${userId}`,
                data: data
            })
        }
    })
}

module.exports = {
    createAlbum,
    getAllAlbum,
    getAlbumById,
    updateAlbumById,
    deleteAlbumById,
    getAlbumOfUser
}