const mongoose = require('mongoose');

const userModel = require('../models/userModel');

// Create User
const createUser = (request, response) => {
    let body = request.body;

    //
    if (!body.address.geo.lat) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Lat is not valid'
        })
    }
    if (!body.address.geo.lng) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Lng is not valid'
        })
    }
    if (!body.address.street) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Street is not valid'
        })
    }
    if (!body.address.suite) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Suite is not valid'
        })
    }
    if (!body.address.city) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'City is not valid'
        })
    }
    if (!body.address.zipcode) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Zipcode is not valid'
        })
    }
    if (!body.company.name) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Name is not valid'
        })
    }
    if (!body.company.catchPhrase) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Catch Phrase is not valid'
        })
    }
    if (!body.company.bs) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Bs is not valid'
        })
    }
    if (!body.name) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Name is not valid'
        })
    }
    if (!body.username) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User Name is not valid'
        })
    }
    if (!body.phone) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Phone is not valid'
        })
    }
    if (!body.website) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Website is not valid'
        })
    }
    let newUser = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        username: body.username,
        address: {
            street: body.address.street,
            suite: body.address.suite,
            city: body.address.city,
            zipcode: body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                lng: body.address.geo.lng
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    }
    if (body.address.geo.lat !== undefined) {
        newUser.address.geo.lat = body.address.geo.lat
    }
    if (body.address.geo.lng !== undefined) {
        newUser.address.geo.lng = body.address.geo.lng
    }
    if (body.address.street !== undefined) {
        newUser.address.street = body.address.street
    }
    if (body.address.suite !== undefined) {
        newUser.address.suite = body.address.suite
    }
    if (body.address.city !== undefined) {
        newUser.address.city = body.address.city
    }
    if (body.address.zipcode !== undefined) {
        newUser.address.zipcode = body.address.zipcode
    }
    if (body.company.name !== undefined) {
        newUser.company.name = body.company.name
    }
    if (body.company.catchPhrase !== undefined) {
        newUser.company.catchPhrase = body.company.catchPhrase
    }
    if (body.company.bs !== undefined) {
        newUser.company.bs = body.company.bs
    }
    if (body.name !== undefined) {
        newUser.name = body.name
    }
    if (body.username !== undefined) {
        newUser.username = body.username
    }
    if (body.phone !== undefined) {
        newUser.phone = body.phone
    }
    if (body.website !== undefined) {
        newUser.website = body.website
    }
    userModel.create(newUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        return response.status(200).json({
            status: 'Create new user successfully',
            data: data
        })
    })
}

// Get All User
const getAllUser = (request, response) => {
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        return response.status(200).json({
            status: 'Get all users successfully',
            data: data
        })
    })
}

// Get User By ID
const getUserById = (request, response) => {
    let userId = request.params.userId;

    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User Id is not valid'
        })
    }
    userModel.findById(userId, (error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get user by id ${userId} successfully`,
            data: data
        })

    })
}

// Update User By Id
const updateUser = (request, response) => {
    let userId = request.params.userId;
    let body = request.body;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User Id is not valid'
        })
    }
    if (!body.address.geo.lat) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Lat is not valid'
        })
    }
    if (!body.address.geo.lng) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Lng is not valid'
        })
    }
    if (!body.address.street) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Street is not valid'
        })
    }
    if (!body.address.suite) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Suite is not valid'
        })
    }
    if (!body.address.city) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'City is not valid'
        })
    }
    if (!body.address.zipcode) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Zipcode is not valid'
        })
    }
    if (!body.company.name) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Name is not valid'
        })
    }
    if (!body.company.catchPhrase) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Catch Phrase is not valid'
        })
    }
    if (!body.company.bs) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Bs is not valid'
        })
    }
    if (!body.name) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Name is not valid'
        })
    }
    if (!body.username) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User Name is not valid'
        })
    }
    if (!body.phone) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Phone is not valid'
        })
    }
    if (!body.website) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Website is not valid'
        })
    }
    let updateUser = {
        name: body.name,
        username: body.username,
        address: {
            street: body.address.street,
            suite: body.address.suite,
            city: body.address.city,
            zipcode: body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                lng: body.address.geo.lng
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    }
    if (body.address.geo.lat !== undefined) {
        updateUser.address.geo.lat = body.address.geo.lat
    }
    if (body.address.geo.lng !== undefined) {
        updateUser.address.geo.lng = body.address.geo.lng
    }
    if (body.address.street !== undefined) {
        updateUser.address.street = body.address.street
    }
    if (body.address.suite !== undefined) {
        updateUser.address.suite = body.address.suite
    }
    if (body.address.city !== undefined) {
        updateUser.address.city = body.address.city
    }
    if (body.address.zipcode !== undefined) {
        updateUser.address.zipcode = body.address.zipcode
    }
    if (body.company.name !== undefined) {
        updateUser.company.name = body.company.name
    }
    if (body.company.catchPhrase !== undefined) {
        updateUser.company.catchPhrase = body.company.catchPhrase
    }
    if (body.company.bs !== undefined) {
        updateUser.company.bs = body.company.bs
    }
    if (body.name !== undefined) {
        updateUser.name = body.name
    }
    if (body.username !== undefined) {
        updateUser.username = body.username
    }
    if (body.phone !== undefined) {
        updateUser.phone = body.phone
    }
    if (body.website !== undefined) {
        updateUser.website = body.website
    }
    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update user is successfully",
            updateUser: data
        })

    })
}
// Delete User By Id
const deleteUser = (request, response) => {
    let userId = request.params.userId;

    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'User Id is not valid'
        })
    }

    userModel.findByIdAndDelete(userId, (error) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        return response.status(200).json({
            status: `Delete user with id ${userId} successfully`
        })
    })
}


module.exports = {
    createUser,
    getAllUser,
    updateUser,
    getUserById,
    deleteUser
}