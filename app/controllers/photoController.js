const mongoose = require('mongoose');

const photoModel = require('../models/photoModel');

const createPhoto = (request, response) => {
    let bodyRequest = request.body;

    if (!bodyRequest.albumId) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'AlbumId is required'
        })
    }
    if (!bodyRequest.title) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Title is required'
        })
    }
    if (!bodyRequest.url) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Url is required'
        })
    }
    if (!bodyRequest.thumbnailUrl) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Thumbnail Url is required'
        })
    }

    let newPhoto = {
        _id: mongoose.Types.ObjectId(),
        albumId: bodyRequest.albumId,
        title: bodyRequest.title,
        url: bodyRequest.url,
        thumbnailUrl: bodyRequest.thumbnailUrl
    }

    photoModel.create(newPhoto, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Success: Create Photo successfully',
                data: data
            })
        }
    })
}

const getAllPhoto = (request, response) => {
    let albumId = request.query.albumId;

    let condition = {};

    if (albumId) {
        condition.albumId = albumId
    }

    photoModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Success: Get All Photo',
                data: data
            })
        }
    })
}

const getPhotoById = (request, response) => {
    let photoId = request.params.photoId;

    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Photo ID is not valid'
        })
    }

    photoModel.findById(photoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Get Photo ${photoId}`,
                data: data
            })
        }
    })
}

const updatePhotoById = (request, response) => {
    let photoId = request.params.photoId;
    let bodyRequest = request.body;

    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Photo ID is not a valid'
        })
    }

    let updatePhoto = {
        albumId: bodyRequest.albumId,
        title: bodyRequest.title,
        url: bodyRequest.url,
        thumbnailUrl: bodyRequest.thumbnailUrl
    }

    photoModel.findByIdAndUpdate(photoId, updatePhoto, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Update Photo ${photoId}`,
                data: data
            })
        }
    })
}

const deletePhotoById = (request, response) => {
    let photoId = request.params.photoId;

    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Photo ID is not a valid'
        })
    }

    photoModel.findByIdAndDelete(photoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Delete Photo ${photoId}`,
            })
        }
    })
}

// Get Photos of User
const getPhotoOfAlbum = (request, response) => {
    let albumId = request.params.albumId;

    let condition = {};

    if (albumId) {
        condition.albumId = albumId;
    }

    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Album ID is not a valid'
        })
    }

    photoModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Get All Photo Of User ID: ${albumId}`,
                data: data
            })
        }
    })
}

module.exports = {
    createPhoto,
    getAllPhoto,
    getPhotoById,
    updatePhotoById,
    deletePhotoById,
    getPhotoOfAlbum
}