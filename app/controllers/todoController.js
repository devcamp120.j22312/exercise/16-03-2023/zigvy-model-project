const mongoose = require('mongoose');

const todoModel = require('../models/todoModel');

const createTodo = (request, response) => {
    let bodyRequest = request.body;

    if (!bodyRequest.userId) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'AlbumId is required'
        })
    }
    if (!bodyRequest.title) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Title is required'
        })
    }
    if (!bodyRequest.completed) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Url is required'
        })
    }

    let newTodo = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        completed: bodyRequest.completed
    }

    todoModel.create(newTodo, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Success: Create Todo successfully',
                data: data
            })
        }
    })
}

const getAllTodo = (request, response) => {
    let userId = request.query.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId
    }

    todoModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: 'Success: Get All Todo',
                data: data
            })
        }
    })
}

const getTodoById = (request, response) => {
    let todoId = request.params.todoId;

    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Todo ID is not valid'
        })
    }

    todoModel.findById(todoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Get Todo ${todoId}`,
                data: data
            })
        }
    })
}

const updateTodoById = (request, response) => {
    let todoId = request.params.todoId;
    let bodyRequest = request.body;

    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Todo ID is not a valid'
        })
    }

    let updateTodo = {
        userId: bodyRequest.userId,
        title: bodyRequest.title,
        completed: bodyRequest.completed
    }

    todoModel.findByIdAndUpdate(todoId, updateTodo, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Update Todo ${todoId}`,
                data: data
            })
        }
    })
}

const deleteTodoById = (request, response) => {
    let todoId = request.params.todoId;

    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Todo ID is not a valid'
        })
    }

    todoModel.findByIdAndDelete(todoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Delete Todo ${todoId}`,
            })
        }
    })
}

// Get Todos of User
const getTodoOfUser = (request, response) => {
    let userId = request.params.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'User ID is not a valid'
        })
    }

    todoModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                status: `Success: Get All Todo Of User ID: ${userId}`,
                data: data
            })
        }
    })
}

module.exports = {
    createTodo,
    getAllTodo,
    getTodoById,
    updateTodoById,
    deleteTodoById,
    getTodoOfUser
}