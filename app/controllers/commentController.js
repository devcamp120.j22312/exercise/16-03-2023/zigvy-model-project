const mongoose = require('mongoose');

const commentModel = require('../models/commentModel');

const createComment = (request, response) => {
    let body = request.body;

    if (!body.name) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Name is not valid'
        })
    }
    if (!body.email) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Email is not valid'
        })
    }
    if (!body.body) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Body is not valid'
        })
    }

    let newComment = {
        _id: mongoose.Types.ObjectId(),
        postId: body.postId,
        name: body.name,
        email: body.email,
        body: body.body
    }
    commentModel.createO(newComment, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: 'Success: Create Comment successfully',
                data: data
            })
        }
    })
}

const getAllComment = (request, response) => {
    let postId = request.query.postId;

    let condition = {};

    if (postId) {
        condition.postId = postId;
    }

    commentModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: 'Success: Get All Comment successfully',
                data: data
            })
        }
    })
}

const getCommentById = (request, response) => {
    let commentId = request.params.commentId;

    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: 'Error 400: Bad request',
            message: 'Comment ID is not valid'
        })
    }
    commentModel.findById(commentId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: 'Success: Get Comment By Id successfully',
                data: data
            })
        }
    })
}

const updateCommentById = (request, response) => {
    let commentId = request.params.commentId;
    let bodyRequest = request.body;

    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Comment ID is not a valid'
        })
    }

    let updateComment = {
        postId: bodyRequest.postId,
        name: bodyRequest.name,
        email: bodyRequest.email,
        body: bodyRequest.body
    }

    commentModel.findByIdAndUpdate(commentId, updateComment, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Success Update Comment successfully',
                data: data
            })
        }
    })
}

const deleteCommentById = (request, response) => {
    let commentId = request.params.commentId;

    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Comment ID is not a valid'
        })
    }

    commentModel.findByIdAndDelete(commentId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Success: Delete Comment By Id successfully',
            })
        }
    })
}

const getCommentsOfPost = (request, response) => {
    let postId = request.params.postId;

    let condition = {};

    if(postId) {
        condition.postId = postId;
    }

    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: 'Error 400: Bad Request',
            message: 'Comment ID is not a valid'
        })
    }

    commentModel.find(condition, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Error 500: Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Success: Get ALL Comment successfully',
                data: data
            })
        }
    })
}

module.exports = {
    createComment,
    getAllComment,
    getCommentById,
    updateCommentById,
    deleteCommentById,
    getCommentsOfPost
}