const express = require('express');

const todoMiddleware = require('../middlewares/todoMiddleware');

const todoController = require('../controllers/todoController');

const todoRouter = express.Router();

todoRouter.post('/todos', todoMiddleware.todoMiddleware, todoController.createTodo);

todoRouter.get('/todos', todoMiddleware.todoMiddleware, todoController.getAllTodo);

todoRouter.put('/todos/:todoId', todoMiddleware.todoMiddleware, todoController.updateTodoById);

todoRouter.get('/todos/:todoId', todoMiddleware.todoMiddleware, todoController.getTodoById);

todoRouter.delete('/todos/:todoId', todoMiddleware.todoMiddleware, todoController.deleteTodoById);

todoRouter.get('/users/:userId/todos', todoMiddleware.todoMiddleware, todoController.getTodoOfUser);

module.exports = todoRouter;