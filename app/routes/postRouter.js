const express = require('express');

const postMiddleware = require('../middlewares/postMiddleware');

const postController = require('../controllers/postController');

const postRouter = express.Router();

postRouter.post('/posts', postMiddleware.postMiddleware, postController.createPost);

postRouter.get('/posts', postMiddleware.postMiddleware, postController.getAllPost);

postRouter.put('/posts/:postId', postMiddleware.postMiddleware, postController.updatePostById);

postRouter.get('/posts/:postId', postMiddleware.postMiddleware, postController.getPostById);

postRouter.delete('/posts/:postId', postMiddleware.postMiddleware, postController.deletePostById);

postRouter.get('/users/:userId/posts', postMiddleware.postMiddleware, postController.getPostsOfUser);

module.exports = postRouter;