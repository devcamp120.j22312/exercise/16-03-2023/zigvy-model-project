const express = require('express');

const commentMiddleware = require('../middlewares/commentMiddleware');

const commentController = require('../controllers/commentController');

const commentRouter = express.Router();

commentRouter.post('/comments', commentMiddleware.commentMiddleware, commentController.createComment);

commentRouter.get('/comments', commentMiddleware.commentMiddleware, commentController.getAllComment);

commentRouter.put('/comments/:commentId', commentMiddleware.commentMiddleware, commentController.updateCommentById);

commentRouter.get('/comments/:commentId', commentMiddleware.commentMiddleware, commentController.getCommentById);

commentRouter.delete('/comments/:commentId', commentMiddleware.commentMiddleware, commentController.deleteCommentById);

commentRouter.get('/posts/:postId/comments', commentMiddleware.commentMiddleware, commentController.getCommentsOfPost);

module.exports = commentRouter;