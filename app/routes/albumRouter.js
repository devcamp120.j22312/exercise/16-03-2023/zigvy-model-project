const express = require('express');

const albumMiddleware = require('../middlewares/albumMiddleware');

const albumController = require('../controllers/albumController');

const albumRouter = express.Router();

albumRouter.post('/albums', albumMiddleware.albumMiddleware, albumController.createAlbum);

albumRouter.get('/albums', albumMiddleware.albumMiddleware, albumController.getAllAlbum);

albumRouter.get('/albums/:albumId', albumMiddleware.albumMiddleware, albumController.getAlbumById);

albumRouter.put('/albums/:albumId', albumMiddleware.albumMiddleware, albumController.updateAlbumById);

albumRouter.delete('/albums/:albumId', albumMiddleware.albumMiddleware, albumController.deleteAlbumById);

albumRouter.get('/users/:userId/albums', albumMiddleware.albumMiddleware, albumController.getAlbumOfUser);

module.exports =  albumRouter ;