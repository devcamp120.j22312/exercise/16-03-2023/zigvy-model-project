const express = require('express');

const photoMiddleware = require('../middlewares/photoMiddleware');

const photoController = require('../controllers/photoController');

const photoRouter = express.Router();

photoRouter.post('/photos', photoMiddleware.photoMiddleware, photoController.createPhoto);

photoRouter.get('/photos', photoMiddleware.photoMiddleware, photoController.getAllPhoto);

photoRouter.put('/photos/:photoId', photoMiddleware.photoMiddleware, photoController.updatePhotoById);

photoRouter.get('/photos/:photoId', photoMiddleware.photoMiddleware, photoController.getPhotoById);

photoRouter.delete('/photos/:photoId', photoMiddleware.photoMiddleware, photoController.deletePhotoById);

photoRouter.get('/albums/:albumId/photos', photoMiddleware.photoMiddleware, photoController.getPhotoOfAlbum);

module.exports = photoRouter;