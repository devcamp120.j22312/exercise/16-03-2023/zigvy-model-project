const express = require('express');

const userMiddleware = require('../middlewares/userMiddleware');

const userController = require('../controllers/userController');

const userRouter = express.Router();

userRouter.post('/users', userMiddleware.userMiddleware, userController.createUser);

userRouter.get('/users', userMiddleware.userMiddleware, userController.getAllUser);

userRouter.put('/users/:userId', userMiddleware.userMiddleware, userController.updateUser);

userRouter.get('/users/:userId', userMiddleware.userMiddleware, userController.getUserById);

userRouter.delete('/users/:userId', userMiddleware.userMiddleware, userController.deleteUser);

module.exports = userRouter;