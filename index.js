// Khai báo thư viện 
const express = require('express');
const mongoose = require('mongoose');

// Khai báo port
const port = 8000;

const userModel = require('./app/models/userModel');
const postModel = require('./app/models/postModel');
const commentModel = require('./app/models/commentModel');
const albumModel = require('./app/models/albumModel');
const photoModel = require('./app/models/photoModel');
const todoModel = require('./app/models/todoModel');

// Import router
const userRouter = require('./app/routes/userRouter');
const postRouter = require('./app/routes/postRouter');
const commentRouter = require('./app/routes/commentRouter');
const photoRouter = require('./app/routes/photoRouter');
const albumRouter = require('./app/routes/albumRouter');
const todoRouter = require('./app/routes/todoRouter');

// Khai báo app
const app = express();

// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

mongoose.connect("mongodb://127.0.0.1/Zigvy_Interview", (err) => {
    if (err) {
        throw err;
    }

    console.log("Connect MongoDB Successfully!");
})

app.use('/', userRouter);
app.use('/', postRouter);
app.use('/', commentRouter);
app.use('/', photoRouter);
app.use('/', albumRouter);
app.use('/', todoRouter);

// Khởi động app
app.listen(port, () => {
    console.log("App listening on port: ", port);
})